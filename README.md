<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Ligne de commande Unix

[*Ligne de commande Unix*](https://gitlab.com/vigou3/ligne-commande-unix/-/releases/) est un laboratoire (ou atelier) d'introduction à la ligne de commande Unix offert dans le cadre du cours [IFT-1902 Programmation avec R pour l'analyse de données](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/ift-1902-informatique-pour-actuaires.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

La ligne de commande est la plus ancienne des interfaces avec les ordinateurs. Si les interfaces graphiques ont à plusieurs égards grandement facilité l'interaction humain-machine, elles n'ont pas pour autant fait disparaitre ou rendu obsolète la ligne de commande.

Le laboratoire consiste en une présentation entrecoupée d'exemples et d'exercices.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Ligne de commande Unix» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`ligne-commande-unix`](https://gitlab.com/vigou3/ligne-commande-unix) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`ligne-commande-unix-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/ligne-commande-unix-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `ligne-commande-unix-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
