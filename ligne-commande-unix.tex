%%% Copyright (C) 2018-2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Ligne de commande Unix»
%%% https://gitlab.com/vigou3/ligne-commande-unix
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{applekeys}                 % \tabkey et al.
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{listings}                  % code source
  \usepackage{pict2e}                    % schéma redirection
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Ligne de commande Unix}
  \author{Vincent Goulet}
  \institute{Professeur titulaire \\
    École d'actuariat, Université Laval}
  \renewcommand{\year}{2024}
  \renewcommand{\month}{08}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/ligne-commande-unix}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \colorlet{codebg}{LightYellow1}           % fond code R
  \colorlet{prompt}{Orchid4}                % invite de commande
  \colorlet{alert}{mLightBrown} % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias de couleur Metropolis
  \colorlet{code}{mLightGreen}  % alias de couleur Metropolis

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Rapports dynamiques avec Shiny},
    colorlinks = {true},
    linktocpage = {true},
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Sections de code source
  \lstloadlanguages{bash}
  \lstset{language=bash,
    basicstyle=\ttfamily\NoAutoSpacing,
    keywordstyle=\mdseries,
    commentstyle=\color{comments}\slshape,
    emphstyle=\bfseries,
    moredelim=[is][\color{prompt}]{---}{-}, % éviter conflit avec ls -l
    escapeinside=`',
    extendedchars=true,
    showstringspaces=false,
    backgroundcolor=\color{codebg},
    frame=leftline,
    framerule=2pt,
    framesep=5pt,
    xleftmargin=7.4pt
  }

  %% =========================
  %%  Nouveaux environnements
  %% =========================

  %% Listes de commandes
  \newenvironment{ttscript}[1]{%
    \begin{list}{}{%
        \setlength{\labelsep}{1.5ex}
        \settowidth{\labelwidth}{\texttt{#1}}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlength{\parsep}{0.5ex plus0.2ex minus0.2ex}
        \setlength{\itemsep}{0.3ex}
        \renewcommand{\makelabel}[1]{\vphantom{|}##1\hfill}}}
    {\end{list}}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Boite additionnelle (basées sur awesomebox.sty) pour remarques
  %% spécifiques à Windows.
  \newcommand{\windowsbox}[1]{%
    \awesomebox{\aweboxrulewidth}{\faWindows}{black}{#1}}

  %% Renvois vers vidéos YouTube
  \newcommand{\video}[2]{%
    \begin{center}
      \href{#1}{%
        \makebox[5mm]{\raisebox{-2pt}{\Large\faYoutube}}\;{#2}}
    \end{center}}

  %% Renvoi vers GitLab sur la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \usepackage{relsize}
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\smaller\faExternalLink*}%
    \else
      \href{#1}{#2~\smaller\faExternalLink*}%
    \fi
  }

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs utilisées pour composer les couvertures
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}[plain]
  \centering
  \begin{minipage}{0.8\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/tar} \\
    \footnotesize Tiré de \href{https://xkcd.com/1168/}{XKCD.com}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Sommaire}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{Objectif}

  Utiliser la ligne de commande Unix pour effectuer des
  opérations simples dans le système de fichiers.
\end{frame}

%% mainmatter
\section{Introduction}

\begin{frame}
  \frametitle{Un outil puissant toujours pertinent}

  La ligne de commande du système d'exploitation demeure une interface
  importante pour les programmeurs.
  \begin{itemize}
  \item Parfois \alert{plus simple} qu'une interface graphique
  \item Souvent \alert{plus rapide} qu'une interface graphique
  \item Parfois la \alert{seule option} (notamment pour les
    utilitaires Unix comme \code{grep}, \code{sed}, \code{awk})
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Un peu de terminologie}

  \begin{description}
  \item[(Interface en) Ligne de commande] \mbox{} \\
    Mode d'interaction avec un programme informatique dans lequel
    l'utilisateur dicte les commandes et reçoit les réponses de
    l'ordinateur en mode texte (\emph{command line interface}, CLI)
  \item[Interpréteur de commandes] \mbox{} \\
    Programme qui gère l'interface en ligne de commande
    (\emph{terminal}, \emph{shell})
  \item[Invite de commande] \mbox{} \\
    Symbole affiché par l'interpréteur de commande pour indiquer qu'il
    est prêt à recevoir une commande (\emph{command prompt})
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Ligne de commande Windows}

  \begin{minipage}[t]{0.45\linewidth}
    \begin{block}{\texttt{cmd.exe}}
      \vskip1em
      \begin{itemize}
      \item Accessoires | Invite de commandes
      \item pas très puissant
      \item invite par défaut: \code{\color{prompt}C:\string\>}
      \end{itemize}
      \includegraphics[width=\linewidth,keepaspectratio]{images/cmd}
    \end{block}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.45\linewidth}
    \begin{block}{Git Bash ou MSYS2}
      \vskip1em
      \begin{itemize}
      \item interpréteur Bash très puissant
      \item standard Unix
      \item invite par défaut: \code{\color{prompt}\$}
      \end{itemize}
      \includegraphics[width=\linewidth,keepaspectratio]{images/git-bash}
    \end{block}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Ligne de commande macOS}

  \begin{block}{Terminal}
    \begin{itemize}
    \item Applications | Utilitaires | Terminal ou via Spotlight
    \item interpréteur Bash ou Z Shell (depuis macOS 10.15
      Catalina)
    \item invite par défaut: \code{\color{prompt}\%}
    \end{itemize}
    \vspace{-\baselineskip}
    \begin{minipage}[t]{0.45\linewidth}
      \mbox{} \\
      \includegraphics[width=\linewidth]{images/terminal}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.52\linewidth}
      \small%
      \tipbox{Vous pouvez modifier l'apparence de la ligne de
        commande dans les préférences de l'application Terminal}
    \end{minipage}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Rudiments de la ligne de commande}

  Nous allons uniquement étudier les commandes qui permettent de:
  \begin{itemize}
  \item se déplacer dans le système de fichiers
  \item afficher la liste des fichiers d'un répertoire
  \item afficher le contenu d'un fichier
  \item copier et supprimer un fichier
  \end{itemize}

  \warningbox{Commandes des interpréteurs de commande Unix (de type
    Bash) seulement.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Quelques Unix-ismes}

  \begin{itemize}
  \item Répertoire par défaut est normalement le \alert{répertoire
      personnel} (\emph{home directory}) \\
    \medskip
    \begin{minipage}{0.45\linewidth}
      \small Windows
      \vspace{-1ex}
\begin{lstlisting}
---$- printenv HOME
/c/Users/vincent
\end{lstlisting}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\linewidth}
      \small macOS
      \vspace{-1ex}
\begin{lstlisting}
---$- printenv HOME
/Users/vincent
\end{lstlisting}
    \end{minipage}
  \item Répertoire personnel identifié par le symbole
    «\code{\string~}»
  \item Noms de répertoires séparés par la barre oblique «\code{/}»
  \item Ligne de commande sensible à la casse (\code{foo} $\neq$
    \code{Foo} $\neq$ \code{FOO})
  \item Fichiers \code{.foo} cachés dans la liste des fichiers
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \windowsbox{Git Bash et MSYS2 utilisent la nomenclature Unix pour
    identifier les disques et les répertoires.
    \begin{center}
      \begin{tabular}{ll}
        \textbf{Windows} & \textbf{Unix} \\
        \code{C:} & \code{/c} \\
        \code{C:{\textbackslash}Users} & \code{/c/Users} \\
        \code{C:{\textbackslash}Users{\textbackslash}vincent} & \code{/c/Users/vincent} \\
        \dots \\
      \end{tabular}
    \end{center}
    }
\end{frame}

\begin{frame}[plain]
  \tipbox{Gagnez en efficacité au clavier!
    \begin{ttscript}{Ctrl-M}
    \item[$\uparrow$ | $\downarrow$] commande
      précédente~|~suivante dans l'historique
    \item[\code{\tabkey}] autocomplétion de la commande, du nom de
      fichier, etc. \newline
      \alert{votre meilleure alliée!}
    \item[\texttt{Ctrl-R}] recherche dans l'historique des commandes
    \item[\texttt{Ctrl-K}] suppression du texte qui suit le curseur
    \item[\texttt{Ctrl-U}] suppression du texte qui précède le curseur
    \end{ttscript}
    Il y en a
    \link{https://en.wikipedia.org/wiki/GNU_Readline}{plusieurs autres}.%
  }
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  \begin{enumerate}
  \item Démarrer la ligne de commande de votre système d'exploitation
  \item Identifier le répertoire courant dans l'invite de commande
  \item Repérer ce répertoire dans le système de fichier avec
    l'Explorateur Windows ou le Finder (garder la fenêtre ouverte)
  \end{enumerate}
\end{frame}


\section[Navigation dans le système \\ de fichiers]{Navigation dans le système de fichiers}

\begin{frame}[fragile=singleslide]
  \frametitle{Afficher le répertoire courant}

  \code{pwd} \quad (\emph{print working directory})
  \begin{itemize}
  \item affiche le chemin d'accès absolu du répertoire courant

\begin{lstlisting}
---~$- pwd
/Users/vincent
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Changer de répertoire}

  \code{cd} \quad (\emph{change directory})
  \begin{itemize}
  \item change le répertoire courant pour
    celui donné en argument
  \item argument peut être un chemin d'accès \alert{absolu} ou
    \alert{relatif}
  \item nom fictif «\code{.}» identifie le répertoire courant
  \item nom fictif «\code{..}» identifie le parent du répertoire
    courant
  \item sans argument, ramène au répertoire personnel «\code{\string~}»

\begin{lstlisting}
---~$- cd Desktop/
/Users/vincent/Desktop
---~/Desktop$- cd ~/Documents/cours/
---~/Documents/cours$- cd ..
---~/Documents$- cd
---~$-
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \warningbox{Si le nom d'un répertoire contient des espaces, il faut
    les «désactiver» avec la barre oblique inversée: \code{cd
      Program{\textbackslash} Files}.}

  \tipbox{Dès qu'un nom contient des espaces, utilisez la touche
    {\tabkey} pour compléter le nom. Les symboles
    \code{\textbackslash} seront ajoutés automatiquement.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Lister les fichiers}

  \code{ls} \quad (\emph{list})
  \begin{itemize}
  \item affiche les fichiers du répertoire en argument
  \item sans argument, affiche les fichiers du répertoire courant

\begin{lstlisting}
---~$- ls
Desktop
Documents
Downloads
<...>
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice (1/2)}

  \begin{enumerate}
  \item Afficher le répertoire courant. Comparer avec celui mentionné
    dans l'invite de commande.
\begin{lstlisting}
---~$- pwd
\end{lstlisting}
  \item Afficher la liste des fichiers du répertoire courant. Comparer
    avec la liste de l'Explorateur Windows ou du Finder.
\begin{lstlisting}
---~$- ls
\end{lstlisting}
  \item Choisir un sous-répertoire du répertoire courant que vous
    savez contenir lui-même un sous-répertoire (par exemple:
    \code{Documents/Cours}).
  \item Afficher, \alert{sans d'abord s'y déplacer}, la liste des
    fichiers du premier sous-répertoire (\code{Documents}), puis celle
    du second (\code{Cours}).
\begin{lstlisting}
---~$- ls Documents/Cours
\end{lstlisting}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice (2/2)}

  \begin{enumerate}
    \setcounter{enumi}{4}
  \item Faire du sous-sous-répertoire ci-dessus (\code{Cours}) le
    répertoire courant.
\begin{lstlisting}
---~$- cd Documents/Cours
\end{lstlisting}
  \item Afficher une liste détaillée des fichiers du nouveau
    répertoire courant avec la commande \code{ls~-l}.
\begin{lstlisting}
---~$- ls -l
\end{lstlisting}
  \item Revenir au répertoire personnel avec une seule commande.
\begin{lstlisting}
---~$- cd ~
\end{lstlisting}
    ou tout simplement
\begin{lstlisting}
---~$- cd
\end{lstlisting}
  \item Afficher la liste de tous les fichiers du répertoire
    personnel, y compris les fichiers cachés, avec la commande
    \code{ls~-a}.
\begin{lstlisting}
---~$- ls -a
\end{lstlisting}
  \end{enumerate}
\end{frame}


\section{Gestion des fichiers}

\begin{frame}[fragile=singleslide]
  \frametitle{Afficher le contenu d'un fichier}

  \code{cat} \quad (\emph{catenate})
  \begin{itemize}
  \item affiche le contenu du fichier donné en argument
  \item utile uniquement pour les fichiers en texte brut

\begin{lstlisting}
---$- cat hello.txt
Hello World!
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Copier un fichier}

  \code{cp} \quad (\emph{copy})
  \begin{itemize}
  \item copie le fichier en premier argument vers la destination en
    second argument
  \item second argument peut être un nom de répertoire, un nom de
    fichier ou les deux

\begin{lstlisting}
---$- cp hello.txt foo.txt
---$- cp hello.txt Documents/
---$- cp hello.txt Documents/foo.txt
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Supprimer un fichier}

  \code{rm} \quad (\emph{remove})
  \begin{itemize}
  \item supprime le ou les fichiers en argument
  \item fichiers disparus à jamais

\begin{lstlisting}
---$- rm foo
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice (1/2)}

  Faire de votre répertoire personnel le répertoire courant et créer
  un fichier (vide) nommé \code{foobar} avec la commande:
\begin{lstlisting}
---$- touch foobar
\end{lstlisting}

  Effectuer les opérations ci-dessous en validant chaque fois leur
  effet dans l'Explorateur Windows ou dans le Finder.
  \begin{enumerate}
  \item Constater que le fichier \code{foobar} est vide: sa taille est
    nulle et l'affichage de son contenu ne retourne rien.
\begin{lstlisting}
---$- ls -l foobar
---$- cat foobar
\end{lstlisting}
  \item Copier le fichier \code{foobar} dans votre répertoire
    personnel sous le nom \code{foo.txt}.
\begin{lstlisting}
---$- cp foobar foo.txt
\end{lstlisting}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice (2/2)}

  \begin{enumerate}
    \setcounter{enumi}{2}
  \item Copier le fichier \code{foobar} dans le répertoire
    \code{\string~/Documents}.
\begin{lstlisting}
---$- cp foobar Documents
\end{lstlisting}
  \item Copier le fichier \code{foobar} dans le répertoire
    \code{\string~/Documents} sous le nom \code{bar.txt}.
\begin{lstlisting}
---$- cp foobar Documents/bar.txt
\end{lstlisting}
  \item Copier le fichier \code{\string~/Documents/bar.txt} dans le
    répertoire courant. (\emph{Astuce}: utiliser un nom de répertoire
    fictif.)
\begin{lstlisting}
---$- cp Documents/bar.txt .
\end{lstlisting}
  \item Supprimer tous les fichiers créés ci-dessus.
\begin{lstlisting}
---$- rm foobar foo.txt bar.txt Documents/bar.txt
\end{lstlisting}
  \end{enumerate}
\end{frame}

\section[Transfert de données \\ et redirection]{Transfert de données et redirection}

\begin{frame}
  \frametitle{Philosophie Unix}

  \alert{«Ne faire qu'une seule chose, et la faire bien.»}

  \begin{itemize}
  \item Écrivez des programmes qui effectuent une seule chose et qui
    le font bien.
  \item Écrivez des programmes qui collaborent.
  \item Écrivez des programmes pour gérer des flux de texte, car c'est
    une interface universelle.
  \end{itemize}

  {\small (Source: Wikipedia)}
\end{frame}

\begin{frame}
  \frametitle{Entrée et sortie standard}

  Les programmes Unix en ligne de commande:
  \begin{itemize}
  \item reçoivent leurs données d'une \alert{entrée standard}
    (\emph{standard input}, stdin)
  \item écrivent leurs résultats vers la \alert{sortie standard}
    (\emph{standard output}, stdout)
  \item émettent leurs erreurs vers l'\alert{erreur standard}
    (\emph{standard error}, stderr)
  \end{itemize}

  \tipbox{Dans un terminal, l'entrée standard est le clavier et la
    sortie et l'erreur standards, l'écran}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Transfert de données}

  \emph{stdout} \code{\textbar} \emph{stdin} \quad («tuyau», \emph{pipe})

  \begin{itemize}
  \item transfère la sortie d'un programme directement à l'entrée d'un
    autre programme
    \begin{center}
      \setlength{\unitlength}{3mm}
      \begin{picture}(30,3)(-0.75,-1.25)
        \thicklines
        \put(0,0){\makebox(0,0){\Large\faStream}}
        \put(2,0.125){\vector(1,0){3}}
        \put(7,0){\makebox(0,0){\Large\faCogs}}
        \put(9,0.125){\vector(1,0){3}}
        \put(10.25,1.0){\makebox(0,0){\code{\textbar}}}
        \put(14,0){\makebox(0,0){\Large\faCogs}}
        \put(16,0.125){\vector(1,0){3}}
        \put(17.25,1.0){\makebox(0,0){\code{\textbar}}}
        \put(21,0){\makebox(0,0){\Large\faCogs}}
        \put(23,0.125){\vector(1,0){3}}
        \put(28,0){\makebox(0,0){\Large\faStream}}
      \end{picture}
    \end{center}
  \item similaire à la composition de fonctions en mathématiques
    \begin{equation*}
      (g \circ f)(x) = g(f(x))
      \quad \Leftrightarrow \quad
      x \mid f() \mid g()
    \end{equation*}
\begin{lstlisting}
---~$- pwd
/Users/vincent
---~$- pwd | tr '[a-z]' '[A-Z]'
/USERS/VINCENT
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Redirection}

  \emph{stdout} \code{>} \emph{fichier}

  \begin{itemize}
  \item redirige la sortie standard d'un programme vers un fichier
    \begin{center}
      \setlength{\unitlength}{3mm}
      \begin{picture}(16,3)(-0.75,-1.25)
        \thicklines
        \put(0,0){\makebox(0,0){\Large\faStream}}
        \put(2,0.125){\vector(1,0){3}}
        \put(7,0){\makebox(0,0){\Large\faCogs}}
        \put(9,0.125){\vector(1,0){3}}
        \put(10.25,1.0){\makebox(0,0){\code{>}}}
        \put(14,0){\makebox(0,0){\Large\faFile*[regular]}}
      \end{picture}
    \end{center}

  \item si le fichier existe déjà, son contenu est écrasé
  \item variante double \code{>>} pour ajouter le contenu à la fin du
    fichier
\begin{lstlisting}
---$- pwd > wd.txt
---$- cat wd.txt
/Users/vincent
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Redirection (suite)}

  \emph{stdin} \code{<} \emph{fichier}

  \begin{itemize}
  \item déverse le contenu d'un fichier dans l'entrée standard d'un
    programme
    \begin{center}
      \setlength{\unitlength}{3mm}
      \begin{picture}(16,3)(-0.75,-1.25)
        \thicklines
        \put(0,0){\makebox(0,0){\Large\faFile*[regular]}}
        \put(2,0.125){\vector(1,0){3}}
        \put(3.25,1.0){\makebox(0,0){\code{<}}}
        \put(7,0){\makebox(0,0){\Large\faCogs}}
        \put(9,0.125){\vector(1,0){3}}
        \put(14,0){\makebox(0,0){\Large\faStream}}
      \end{picture}
    \end{center}
  \item variante double existe aussi

\begin{lstlisting}
---$- tr '[a-z]' '[A-Z]' < wd.txt
/USERS/VINCENT
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sommaire graphique}

  \centering
  \setlength{\unitlength}{4mm}
  \small
  \begin{picture}(31.25,9)(-0.25,-1.5)
    \thicklines
    \put(3,6){\makebox(0,0)[t]{\parbox{6.5\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          entrée standard}}}
    \put(6,5){\vector(1,0){3.5}}
    \put(12,6){\makebox(0,0)[t]{\parbox{6\unitlength}{
          \centering{\LARGE\faCogs} \\[4pt]
          commande}}}
    \put(14.75,5){\line(1,0){1.5}}
    \put(16.25,7){\line(0,-1){4}}
    \put(16.25,7){\vector(1,0){1.75}}
    \put(16.25,3){\vector(1,0){1.75}}
    \put(21,8){\makebox(0,0)[t]{\parbox{6.5\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          erreur standard}}}
    \put(21,4){\makebox(0,0)[t]{\parbox{6\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          sortie standard}}}
    \put(23.75,3){\vector(1,0){2.75}}
    \put(25,3.5){\makebox(0,0){\code{>}}}
    \put(28,4){\makebox(0,0)[t]{\parbox{4\unitlength}{
          \centering{\LARGE\faFile*[regular]} \\[4pt]
            fichier}}}
    \Line(21,0.75)(21,0)(3,0)
    \put(3,0){\vector(0,1){2.5}}
    \put(16.25,0.5){\makebox(0,0){\code{\textbar}}}
    \Line(28,0.75)(28,-1)(12,-1)(12,0)
    \put(25,-0.5){\makebox(0,0){\code{<}}}
  \end{picture}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Créer un fichier nommé \code{foobar} depuis la ligne de commande
  avec la commande:
\begin{lstlisting}
---$- echo 'aaa.bbb.ccc' > foobar
\end{lstlisting}

  Déterminer ensuite le résultat des commandes suivantes.
  \begin{enumerate}
  \item \code{cat foobar}
  \item \code{cat foobar | cut -d . -f 1}
  \item \code{tr a z < foobar}
  \item \code{ls -la \string~ > foobar} (examiner le contenu du fichier avec \code{cat})
  \item \code{rm foobar}
  \end{enumerate}
\end{frame}


\section{Exécution de procédures}

\begin{frame}[fragile=singleslide]
  \frametitle{Procédures d'interpréteur de commandes}

  Les interpréteurs de commandes Unix contiennent un langage de
  programmation afin de créer des procédures --- \emph{shell scripts}
  --- pour automatiser des tâches.

  \begin{itemize}
  \item Simples fichiers texte avec sur la première ligne
    \medskip
    \begin{minipage}{0.45\linewidth}
\begin{lstlisting}
#!/bin/sh
\end{lstlisting}
    \end{minipage}
    \hfill ou \hfill
    \begin{minipage}{0.45\linewidth}
\begin{lstlisting}
#!/bin/bash
\end{lstlisting}
    \end{minipage}
  \item
    \link{https://fr.wikipedia.org/wiki/Permissions_UNIX}{Permissions
      Unix} du fichier doivent inclure le \alert{droit d'exécution}
\begin{lstlisting}
---$- ls -l bonjour.sh
-rw`\textcolor{alert}{x}'r--r--  1 vincent  staff  136  8 nov 09:18 bonjour.sh
\end{lstlisting}
  \item Commande \code{chmod} ajoute le droit d'exécution (ici: pour l'utilisateur seulement)
\begin{lstlisting}
---$- chmod u+x bonjour.sh
\end{lstlisting}
  \item Exécution de la procédure requiert \alert{obligatoirement} le
    chemin d'accès
\begin{lstlisting}
---$- ./bonjour.sh
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Le fichier \code{bonjour.sh} est livré avec cette formation.

  \begin{enumerate}
  \item Faire du répertoire contenant le matériel de la formation le
    répertoire courant.
  \item Afficher le contenu du fichier \code{bonjour.sh}.
\begin{lstlisting}
---$- cat bonjour.sh
\end{lstlisting}
  \item Vérifier les permissions du fichier avec la commande \code{ls
      -l}.
\begin{lstlisting}
---$- ls -l bonjour.sh
\end{lstlisting}
  \item Si nécessaire, ajouter le droit d'exécution au fichier et
    répéter l'étape précédente.
\begin{lstlisting}
---$- chmod u+x bonjour.sh
\end{lstlisting}
  \item Exécuter la procédure \code{bonjour.sh}.
\begin{lstlisting}
---$- ./bonjour.sh
\end{lstlisting}
  \end{enumerate}
\end{frame}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
